<?php
//2. Реализовать любое условие if, которое будет содержать в себе не менее 4 условий
$a = 14;
$b = 18;

if ($a > $b) { 
	echo '<pre>';
	echo "14 > 18";
	echo '<pre/>'; 
}

if ($a < $b) {
	echo '<pre>';
	echo "14 < 18";
	echo '<pre/>';
}

if ($a == $b) {
	echo '<pre>';
	echo "14 = 18";
	echo '<pre/>';
}

if ($a >= $b) {
	echo '<pre>';
	echo "14 >= 18";
	echo '<pre/>';
}	

if ($a <= $b) {
	echo '<pre>';
	echo "14 <= 18";
	echo '<pre/>';
}	

?>

<?php
//Создать переменную $count = 0; реализовать цикл while, от $count до 10; вывести на экран значение на каждой второй итерации.
$count = 0;
while($count < 11) {
	echo $count;
	$count++;
}




?>
