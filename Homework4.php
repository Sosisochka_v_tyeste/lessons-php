<?php
//Найти функции и привести по два примера работы с ними
//Функция, которая вернёт все ключи массива - array_keys

$emergency = array(
			"ambulance" => "03",
			"fireman" => "01",
			911 => "police"
 );
	echo '<pre>';
	var_dump (array_keys($emergency)); 
	echo '</pre>';

$porsche = array(
		"Boxter" => "30000 euro",
		"Carrera" => "45000 euro",
		911 => "65000 euro"
 );
	echo '<pre>';
	var_dump (array_keys($porsche)); 
	echo '</pre>';

?>


<?php
//Функция, которая оставляет уникальные значения в массиве - array_unique
$cars = array(
	"BMW" => "30000 euro",
	"AUDI" => "30000 euro",
	"VW" => "15000 euro",
	"Mercedes" => "35000 euro"
 );

echo '<pre>';
var_dump (array_unique($cars)); 
echo '</pre>';


$flowers = array(
	"Chrysant" => "75-105 cm",
	"Rose" => "40-80 cm",
	"Lilium" => "75-105 cm"
 );

echo '<pre>';
var_dump (array_unique($flowers)); 
echo '</pre>';

?>

<?php
//Функция, которая подсчитывает количество элементов в массиве - count
$a = array(
			0 => 1,
			1 => 0.1,
			2 => "green onion"
 );
	echo '<pre>';
	var_dump (count($a)); 
	echo '</pre>';

$colour = array(
			0 => "green",
			1 => "blue",
			2 => "orange",
			3 => "red"
 );
	echo '<pre>';
	var_dump (count($colour)); 
	echo '</pre>';

?>


<?php
$dayNumber = date('D');//Создаём переменную и присваиваем ей значение соответствующее системной дате в формате 'Monday'

switch ($dayNumber) { //Создаём управляющую конструкцию с оператором switch
	case 1:
		echo "Monday";
 		break;
	case 'monday':
	case 2:
		echo "Tuesday";
 		break;
	case 'monday':
	case 3:
		echo "Wednesday";
		break;
	case 4:
		echo "Thursday";
		break;
	case 5:
		echo "Friday";
		break;		
	case 6:
		echo "Saturday";
		break;	

 	default:
		echo "Sunday";
		break;
 }
 
?>

