<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- YOUR FAVICO -->
		<link rel="icon" href="">
		<link rel="stylesheet" type="text/css" href="/bootstrap.css" >
		<title><!-- YOUR TITLE --></title>

		<!-- Latest compiled and minified CSS -->
		<!-- BOOTSTRAP CDN CSS FILE LINK -->

		<script src="bootstrap.js"></script>
		<style>
			body {
				padding-top: 50px;
			}
			img.center {
				display: block;
				margin: 0 auto;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Project name</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav"></ul>
				</div>
			</div>
		</nav>

		<div class="container">
			<h1 class="text-center">Goods list</h1>

			<div class="col-sm-3">
				<!-- Таблица, которую вы видете ниже, это элемент 1-го товара. Вам нужно такю сделать для каждого товара -->
				<table class="table table-bordered">
					<tr>
						<td class="text-center" colspan="2">
							<!-- IMAGE 200x200 -->
							<img src="" class="img-thumbnail center">
						</td>
					</tr>
					<tr>
						<td class="text-center" colspan="2">
							<!-- GOODS NAME -->
						</td>
					</tr>
					<tr>
						<td class="text-center">
							<!-- GOODS PRICE -->$
						</td>
						<td class="text-center">
							<!-- GOODS CREATED AT YYYY-MM-DD -->
						</td>
					</tr>
				</table>
			</div>
		</div>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<!-- BOOTSTRAP CDN JS FILE LINK -->
	</body>
</html>