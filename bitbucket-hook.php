<?php
$data = $_POST ?: file_get_contents('php://input');
$data = json_decode($data);
$pullrequest = $data->pullrequest;

if (!isset($pullrequest)) {
    return;
}

if ($pullrequest->destination->branch->name === 'master') {
    echo `git pull origin master`;
}

?>