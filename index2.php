
<?php
//Получить значение REQUEST_URI из $_SERVER
//С помощью switch case вывести на экран одну из строк
// - это страница About
// - это страница Home
// - это страница Goods
//* - Если путь пустой, реализовать вывод как у Home


$path = trim($_SERVER['REQUEST_URI'], '/'); // другой синтаксис, посмотреть как правильно пишется
	
switch ($path) {
	case 'home':
		echo 'это страница home';
		break;
	case 'about':
		echo 'это страница about';
		break;
	case 'goods':
		echo 'это страница goods';
		break;	
}
?>


